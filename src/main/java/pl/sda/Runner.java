package pl.sda;

import pl.sda.enums.Brand;
import pl.sda.model.Notebook;

import java.math.BigDecimal;
import java.time.Year;
import java.util.HashSet;
import java.util.Set;

public class Runner {
    private final static Set<Notebook> notebooks = new HashSet<>();
    private static final Year YEAR_2000 = Year.of(2000);

    static {
        //to jest statyczny blok inicjalizacyjny, wykona sie tylko raz w calej aplikacji
        notebooks.add(new Notebook("Very old name", Brand.ACER, Year.of(1990), BigDecimal.valueOf(5.0), false));
        notebooks.add(new Notebook("L702", Brand.DELL, Year.of(2007), BigDecimal.valueOf(100.0), false));
        notebooks.add(new Notebook("XPS 15", Brand.DELL, Year.of(2018), BigDecimal.valueOf(10000.0), true));
        notebooks.add(new Notebook("Galaxy Book 12", Brand.SAMSUNG, Year.of(2016), BigDecimal.valueOf(3199.0), true));
        notebooks.add(new Notebook("MacBook Pro", Brand.APPLE, Year.of(2018), BigDecimal.valueOf(12099.0), true));
        notebooks.add(new Notebook("MacBook", Brand.DELL, Year.of(2018), BigDecimal.valueOf(5000), true));
        notebooks.add(new Notebook("MacBook Air", Brand.DELL, Year.of(2018), BigDecimal.valueOf(6000), true));
    }
    public void run() {

        System.out.println("For nie Delle:");
        for (Notebook notebook : notebooks) {
            if(!Brand.DELL.equals(notebook.getBrand())) {
                System.out.println(notebook.getName());
            }
        }
        System.out.println("Stream nie Delle:");
        notebooks.stream()
                .filter(notebook -> !Brand.DELL.equals(notebook.getBrand()))
                .forEach(notebook -> System.out.println(notebook.getName()));
        System.out.println("For laptopy po 2000");
        for (Notebook notebook : notebooks) {
            if(YEAR_2000.isBefore(notebook.getYearOfProduction())) {
                System.out.println(notebook.getName());
            }
        }
        System.out.println("Stream laptopy po 2000");
        notebooks.stream()
                .filter(notebook -> YEAR_2000.isBefore(notebook.getYearOfProduction()))
                .forEach(notebook -> System.out.println(notebook.getName()));
        System.out.println("For nazwa ma mniej niz 5 znakow");
        for (Notebook notebook : notebooks) {
            if(notebook.getName().length() < 5) {
                System.out.println(notebook.getName());
            }
        }
        System.out.println("Stream nazwa ma mniej niz 5 znakow");
        notebooks.stream()
                .filter(notebook -> notebook.getName().length() < 5)
                .forEach(notebook -> System.out.println(notebook.getName()));

        System.out.println("For dostepne laptopy");
        for (Notebook notebook : notebooks) {
            if(notebook.isAvailable()) {
                System.out.println(notebook.getName());
            }
        }
        System.out.println("Stream dostepne laptopy");
        notebooks.stream()
                .filter(notebook -> notebook.isAvailable())
                .forEach(notebook -> System.out.println(notebook.getName()));

        System.out.println("For suma cen laptopow Dell");
        BigDecimal sum = BigDecimal.ZERO;
        for (Notebook notebook : notebooks) {
            if(Brand.DELL.equals(notebook.getBrand())) {
                sum = sum.add(notebook.getPrice());
            }
        }
        System.out.println(sum);

        System.out.println("Stream suma cen laptopow Dell");
        BigDecimal streamSum = notebooks.stream()
                .filter(notebook -> Brand.DELL.equals(notebook.getBrand()))
                .map(notebook -> notebook.getPrice())
                .reduce(BigDecimal.ZERO, (a,b) -> a.add(b));
        System.out.println(streamSum);


        System.out.println("For srednia cen laptopow Apple");
        BigDecimal sumApple = BigDecimal.ZERO;
        int counterApple = 0;
        for (Notebook notebook : notebooks) {
            if(Brand.APPLE.equals(notebook.getBrand())) {
                sumApple = sumApple.add(notebook.getPrice());
                counterApple ++;
            }
        }
        if(counterApple == 0) {
            System.out.println("Obliczenie sredniej niemozliwe");
        } else {
            System.out.println(sumApple.divide(BigDecimal.valueOf(counterApple)));
        }

        System.out.println("Stream srednia cen laptopow Apple");
        BigDecimal appleSum = notebooks.stream()
                .filter(notebook -> Brand.APPLE.equals(notebook.getBrand()))
                .map(notebook -> notebook.getPrice())
                .reduce(BigDecimal.ZERO, (a,b) -> a.add(b));

        long appleCount = notebooks.stream()
                .filter(notebook -> Brand.APPLE.equals(notebook.getBrand()))
                .count();

        System.out.println(appleSum.divide(BigDecimal.valueOf(appleCount)));




        System.out.println("For marka laptopow wiecej niz 4 znaki");
        for (Notebook notebook : notebooks) {
            // Brand.DELL.name() == "DELL"
            if(notebook.getBrand().name().length() > 4) {
                System.out.println(notebook.getName());
            }
        }
        System.out.println("Stream marka laptopow wiecej niz 4 znaki");
        notebooks.stream()
                .filter(notebook -> notebook.getBrand().name().length() > 4)
                .forEach(notebook -> System.out.println(notebook.getName()));
    }
}
